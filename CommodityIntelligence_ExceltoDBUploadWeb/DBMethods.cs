﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;

namespace CommodityIntelligence_ExceltoDBUploadWeb
{
    public class DBMethods
    {
        public class FormattedDbEntityValidationException : Exception
        {
            public FormattedDbEntityValidationException(DbEntityValidationException innerException) :
                base(null, innerException)
            {
            }

            public override string Message
            {
                get
                {
                    var innerException = InnerException as DbEntityValidationException;
                    if (innerException != null)
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine();
                        sb.AppendLine();
                        foreach (var eve in innerException.EntityValidationErrors)
                        {
                            sb.AppendLine(string.Format("- Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().FullName, eve.Entry.State));
                            foreach (var ve in eve.ValidationErrors)
                            {
                                sb.AppendLine(string.Format("-- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                    ve.PropertyName,
                                    eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                    ve.ErrorMessage));
                            }
                        }
                        sb.AppendLine();

                        return sb.ToString();
                    }

                    return base.Message;
                }
            }
        }

        public static void UpdateRowinCommoditySummaryTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, CommoditySummary commodityToUpdate, DataRow row)
        {
            commodityToUpdate.Date_Modified = Extensions.ConvertToDateTime(row["Date Modified"].ToString());
            commodityToUpdate.Month = Extensions.ConvertToDateTime(row["Month"].ToString());
            commodityToUpdate.Category = Convert.ToString(row["Category"]);
            commodityToUpdate.Source = Convert.ToString(row["Source"]);
            commodityToUpdate.Unit = Convert.ToString(row["Unit"]);
            commodityToUpdate.Current_Value = Convert.ToDecimal(row["Current Value"]);
            commodityToUpdate.M_o_M_Change = Convert.ToDecimal(row["M-o-M Change"]);
            commodityToUpdate.Market_Commentary = Convert.ToString(row["Market Commentary"]);
            commodityToUpdate.YTD_Average = Convert.ToDecimal(row["YTD Average"]);
            commodityToUpdate.Current_Year_Forecast = Convert.ToDecimal(row["Current Year Forecast"]);
            commodityToUpdate.Current_Year_Budget = Convert.ToDecimal(row["Current Year Budget"]);
            commodityToUpdate.Previous_Year_Average = Convert.ToDecimal(row["Previous Year Average"]);
            commodityToUpdate.C3_month_change = Convert.ToDecimal(row["3-month change"]);
            commodityToUpdate.C6_month_change = Convert.ToDecimal(row["6-month change"]);
            commodityToUpdate.C12_month_change = Convert.ToDecimal(row["12-month change"]);

            commodityIntelligenceDevEntities.CommoditySummaries.Add(commodityToUpdate);
            commodityIntelligenceDevEntities.Entry(commodityToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinCommoditySummaryTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row)
        {
            CommoditySummary commoditySummary = new CommoditySummary { };
            commoditySummary.Date_Modified = Extensions.ConvertToDateTime(row["Date Modified"].ToString());
            commoditySummary.Month = Extensions.ConvertToDateTime(row["Month"].ToString());
            commoditySummary.Category = Convert.ToString(row["Category"]);
            commoditySummary.Commodity = Convert.ToString(row["Commodity"]);
            commoditySummary.Source = Convert.ToString(row["Source"]);
            commoditySummary.Unit = Convert.ToString(row["Unit"]);
            commoditySummary.Current_Value = Convert.ToDecimal(row["Current Value"]);
            commoditySummary.M_o_M_Change = Convert.ToDecimal(row["M-o-M Change"]);
            commoditySummary.Market_Commentary = Convert.ToString(row["Market Commentary"]);
            commoditySummary.YTD_Average = Convert.ToDecimal(row["YTD Average"]);
            commoditySummary.Current_Year_Forecast = Convert.ToDecimal(row["Current Year Forecast"]);
            commoditySummary.Current_Year_Budget = Convert.ToDecimal(row["Current Year Budget"]);
            commoditySummary.Previous_Year_Average = Convert.ToDecimal(row["Previous Year Average"]);
            commoditySummary.C3_month_change = Convert.ToDecimal(row["3-month change"]);
            commoditySummary.C6_month_change = Convert.ToDecimal(row["6-month change"]);
            commoditySummary.C12_month_change = Convert.ToDecimal(row["12-month change"]);

            commodityIntelligenceDevEntities.CommoditySummaries.Add(commoditySummary);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinPolymerTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, Polymer polymerToUpdate, DataRow row, int year, int monthIndex)
        {
            polymerToUpdate.PricePeriodYear = year;
            polymerToUpdate.PricePeriodMonth = monthIndex;
            polymerToUpdate.ABS_white_black = Convert.ToDecimal(row["ABS white/black"]);
            polymerToUpdate.PC_GF = Convert.ToDecimal(row["PC GF"]);
            polymerToUpdate.PP_talc_filled_20_LC = Convert.ToDecimal(row["PP talc filled 20 LC"]);
            polymerToUpdate.PC_transparent = Convert.ToDecimal(row["PC transparent"]);
            polymerToUpdate.Benzene = Convert.ToDecimal(row["Benzene"]);
            polymerToUpdate.Ethylene = Convert.ToDecimal(row["Ethylene"]);
            polymerToUpdate.Propylene = Convert.ToDecimal(row["Propylene"]);
            polymerToUpdate.Styrene = Convert.ToDecimal(row["Styrene"]);

            commodityIntelligenceDevEntities.Polymers.Add(polymerToUpdate);
            commodityIntelligenceDevEntities.Entry(polymerToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinPolymerTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row, int year, int monthIndex)
        {
            Polymer polymers = new Polymer { };
            polymers.PricePeriodYear = year;
            polymers.PricePeriodMonth = monthIndex;
            polymers.ABS_white_black = Convert.ToDecimal(row["ABS white/black"]);
            polymers.PC_GF = Convert.ToDecimal(row["PC GF"]);
            polymers.PP_talc_filled_20_LC = Convert.ToDecimal(row["PP talc filled 20 LC"]);
            polymers.PC_transparent = Convert.ToDecimal(row["PC transparent"]);
            polymers.Benzene = Convert.ToDecimal(row["Benzene"]);
            polymers.Ethylene = Convert.ToDecimal(row["Ethylene"]);
            polymers.Propylene = Convert.ToDecimal(row["Propylene"]);
            polymers.Styrene = Convert.ToDecimal(row["Styrene"]);

            commodityIntelligenceDevEntities.Polymers.Add(polymers);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinPolyamidesTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, Polyamide polyamidesToUpdate, DataRow row, int year, int monthIndex)
        {
            polyamidesToUpdate.PricePeriodYear = year;
            polyamidesToUpdate.PricePeriodMonth = monthIndex;
            polyamidesToUpdate.Adipic_Acid_NAM__USD_MT_ = Convert.ToDecimal(row["Adipic Acid NAM"]);
            polyamidesToUpdate.Adipic_Acid_NAS__USD_MT_ = Convert.ToDecimal(row["Adipic Acid NAS"]);
            polyamidesToUpdate.Adipic_Acid_EU__USD_MT_ = Convert.ToDecimal(row["Adipic Acid EU"]);
            polyamidesToUpdate.Caprolactam_NAM__USD_MT_ = Convert.ToDecimal(row["Caprolactam NAM"]);
            polyamidesToUpdate.Caprolactam_NAS__USD_MT_ = Convert.ToDecimal(row["Caprolactam NAS"]);
            polyamidesToUpdate.Caprolactam_EU__USD_MT_ = Convert.ToDecimal(row["Caprolactam EU"]);
            polyamidesToUpdate.PA6_NAM__USD_MT_ = Convert.ToDecimal(row["PA6 NAM"]);
            polyamidesToUpdate.PA6_NAS__USD_MT_ = Convert.ToDecimal(row["PA6 NAS"]);
            polyamidesToUpdate.PA6_EU__USD_MT_ = Convert.ToDecimal(row["PA6 EU"]);
            polyamidesToUpdate.PA66_NAM__USD_MT_ = Convert.ToDecimal(row["PA66 NAM"]);
            polyamidesToUpdate.PA66_NAS__USD_MT_ = Convert.ToDecimal(row["PA66 NAS"]);
            polyamidesToUpdate.PA66_EU__USD_MT_ = Convert.ToDecimal(row["PA66 EU"]);

            commodityIntelligenceDevEntities.Polyamides.Add(polyamidesToUpdate);
            commodityIntelligenceDevEntities.Entry(polyamidesToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinPolyamidesTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row, int year, int monthIndex)
        {
            Polyamide polyamides = new Polyamide { };
            polyamides.PricePeriodYear = year;
            polyamides.PricePeriodMonth = monthIndex;
            polyamides.Adipic_Acid_NAM__USD_MT_ = Convert.ToDecimal(row["Adipic Acid NAM"]);
            polyamides.Adipic_Acid_NAS__USD_MT_ = Convert.ToDecimal(row["Adipic Acid NAS"]);
            polyamides.Adipic_Acid_EU__USD_MT_ = Convert.ToDecimal(row["Adipic Acid EU"]);
            polyamides.Caprolactam_NAM__USD_MT_ = Convert.ToDecimal(row["Caprolactam NAM"]);
            polyamides.Caprolactam_NAS__USD_MT_ = Convert.ToDecimal(row["Caprolactam NAS"]);
            polyamides.Caprolactam_EU__USD_MT_ = Convert.ToDecimal(row["Caprolactam EU"]);
            polyamides.PA6_NAM__USD_MT_ = Convert.ToDecimal(row["PA6 NAM"]);
            polyamides.PA6_NAS__USD_MT_ = Convert.ToDecimal(row["PA6 NAS"]);
            polyamides.PA6_EU__USD_MT_ = Convert.ToDecimal(row["PA6 EU"]);
            polyamides.PA66_NAM__USD_MT_ = Convert.ToDecimal(row["PA66 NAM"]);
            polyamides.PA66_NAS__USD_MT_ = Convert.ToDecimal(row["PA66 NAS"]);
            polyamides.PA66_EU__USD_MT_ = Convert.ToDecimal(row["PA66 EU"]);

            commodityIntelligenceDevEntities.Polyamides.Add(polyamides);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinPolyamidesForeCastTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, Polyamides_Forecast polyamidesForeCastToUpdate, DataRow row, int year, int monthIndex)
        {
            polyamidesForeCastToUpdate.PricePeriodYear = year;
            polyamidesForeCastToUpdate.PricePeriodMonth = monthIndex;
            polyamidesForeCastToUpdate.Adipic_Acid_NAM__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid NAM"]);
            polyamidesForeCastToUpdate.Adipic_Acid_NAS__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid NAS"]);
            polyamidesForeCastToUpdate.Adipic_Acid_EU__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid EU"]);
            polyamidesForeCastToUpdate.Caprolactam_NAM__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam NAM"]);
            polyamidesForeCastToUpdate.Caprolactam_NAS__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam NAS"]);
            polyamidesForeCastToUpdate.Caprolactam_EU__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam EU"]);
            polyamidesForeCastToUpdate.PA6_NAM__USD_MT_ = Convert.ToDecimal(row["Average of PA6 NAM"]);
            polyamidesForeCastToUpdate.PA6_NAS__USD_MT_ = Convert.ToDecimal(row["Average of PA6 NAS"]);
            polyamidesForeCastToUpdate.PA6_EU__USD_MT_ = Convert.ToDecimal(row["Average of PA6 EU"]);
            polyamidesForeCastToUpdate.PA66_NAM__USD_MT_ = Convert.ToDecimal(row["Average of PA66 NAM"]);
            polyamidesForeCastToUpdate.PA66_NAS__USD_MT_ = Convert.ToDecimal(row["Average of PA66 NAS"]);
            polyamidesForeCastToUpdate.PA66_EU__USD_MT_ = Convert.ToDecimal(row["Average of PA66 EU"]);

            commodityIntelligenceDevEntities.Polyamides_Forecast.Add(polyamidesForeCastToUpdate);
            commodityIntelligenceDevEntities.Entry(polyamidesForeCastToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinPolyamidesForeCastTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row, int year, int monthIndex)
        {
            Polyamides_Forecast polyamidesforecast = new Polyamides_Forecast { };
            polyamidesforecast.PricePeriodYear = year;
            polyamidesforecast.PricePeriodMonth = monthIndex;
            polyamidesforecast.Adipic_Acid_NAM__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid NAM"]);
            polyamidesforecast.Adipic_Acid_NAS__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid NAS"]);
            polyamidesforecast.Adipic_Acid_EU__USD_MT_ = Convert.ToDecimal(row["Average of Adipic Acid EU"]);
            polyamidesforecast.Caprolactam_NAM__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam NAM"]);
            polyamidesforecast.Caprolactam_NAS__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam NAS"]);
            polyamidesforecast.Caprolactam_EU__USD_MT_ = Convert.ToDecimal(row["Average of Caprolactam EU"]);
            polyamidesforecast.PA6_NAM__USD_MT_ = Convert.ToDecimal(row["Average of PA6 NAM"]);
            polyamidesforecast.PA6_NAS__USD_MT_ = Convert.ToDecimal(row["Average of PA6 NAS"]);
            polyamidesforecast.PA6_EU__USD_MT_ = Convert.ToDecimal(row["Average of PA6 EU"]);
            polyamidesforecast.PA66_NAM__USD_MT_ = Convert.ToDecimal(row["Average of PA66 NAM"]);
            polyamidesforecast.PA66_NAS__USD_MT_ = Convert.ToDecimal(row["Average of PA66 NAS"]);
            polyamidesforecast.PA66_EU__USD_MT_ = Convert.ToDecimal(row["Average of PA66 EU"]);

            commodityIntelligenceDevEntities.Polyamides_Forecast.Add(polyamidesforecast);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinEpoxyResinTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, Epoxy_Composites epoxyresinToUpdate, DataRow row, int year, int monthIndex)
        {
            epoxyresinToUpdate.PricePeriodYear = year;
            epoxyresinToUpdate.PricePeriodMonth = monthIndex;
            epoxyresinToUpdate.Epoxy_Resin__EUR_MT_ = Convert.ToDecimal(row["Epoxy Resin"]);
            epoxyresinToUpdate.Phthalic_Anhydride__EUR_MT_ = Convert.ToDecimal(row["Phtalic Anhydhride"]);
            epoxyresinToUpdate.UP_resin = Convert.ToDecimal(row["UP resin"]);
            epoxyresinToUpdate.Glass_Fibers = Convert.ToDecimal(row["Glass Fibers"]);

            commodityIntelligenceDevEntities.Epoxy_Composites.Add(epoxyresinToUpdate);
            commodityIntelligenceDevEntities.Entry(epoxyresinToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinEpoxyResinTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row, int year, int monthIndex)
        {
            Epoxy_Composites epoxyresin = new Epoxy_Composites { };
            epoxyresin.PricePeriodYear = year;
            epoxyresin.PricePeriodMonth = monthIndex;
            epoxyresin.Epoxy_Resin__EUR_MT_ = Convert.ToDecimal(row["Epoxy Resin"]);
            epoxyresin.Phthalic_Anhydride__EUR_MT_ = Convert.ToDecimal(row["Phtalic Anhydhride"]);
            epoxyresin.UP_resin = Convert.ToDecimal(row["UP resin"]);
            epoxyresin.Glass_Fibers = Convert.ToDecimal(row["Glass Fibers"]);

            commodityIntelligenceDevEntities.Epoxy_Composites.Add(epoxyresin);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinBudgetForcastTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, ForecastData forecastdata, DataRow row)
        {
            forecastdata.MonthYear = Extensions.ConvertToDateTime(row["Month"].ToString());
            forecastdata.Al_MT_Fcast = decimal.Parse(row["Al MT Fcast"].ToString() == string.Empty ? "0.00" : row["Al MT Fcast"].ToString());
            forecastdata.Cu_MT_Fcast = decimal.Parse(row["Cu MT Fcast"].ToString() == string.Empty ? "0.00" : row["Cu MT Fcast"].ToString());
            forecastdata.Silver_Fcast = decimal.Parse(row["Silver Fcast"].ToString() == string.Empty ? "0.00" : row["Silver Fcast"].ToString());
            forecastdata.WTI_Fcast = decimal.Parse(row["WTI Fcast "].ToString() == string.Empty ? "0.00" : row["WTI Fcast "].ToString());
            forecastdata.Brent_Fcast = decimal.Parse(row["Brent Fcast"].ToString() == string.Empty ? "0.00" : row["Brent Fcast"].ToString());
            forecastdata.Steel_HR_MT_Fcast_US = decimal.Parse(row["Steel HR MT Fcast-US"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-US"].ToString());
            forecastdata.Steel_HR_MT_Fcast_EU = decimal.Parse(row["Steel HR MT Fcast-EU"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-EU"].ToString());
            forecastdata.Steel_HR_MT_Fcast_CN = decimal.Parse(row["Steel HR MT Fcast-CN"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-CN"].ToString());
            forecastdata.Steel_HR_MT_Fcast_IN = decimal.Parse(row["Steel HR MT Fcast-IN"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-IN"].ToString());

            commodityIntelligenceDevEntities.ForecastDatas.Add(forecastdata);
            commodityIntelligenceDevEntities.Entry(forecastdata).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinBudgetForcastTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row)
        {
            ForecastData forecastdata = new ForecastData { };
            forecastdata.MonthYear = Extensions.ConvertToDateTime(row["Month"].ToString());
            forecastdata.Al_MT_Fcast = decimal.Parse(row["Al MT Fcast"].ToString() == string.Empty ? "0.00" : row["Al MT Fcast"].ToString());
            forecastdata.Cu_MT_Fcast = decimal.Parse(row["Cu MT Fcast"].ToString() == string.Empty ? "0.00" : row["Cu MT Fcast"].ToString());
            forecastdata.Silver_Fcast = decimal.Parse(row["Silver Fcast"].ToString() == string.Empty ? "0.00" : row["Silver Fcast"].ToString());
            forecastdata.WTI_Fcast = decimal.Parse(row["WTI Fcast "].ToString() == string.Empty ? "0.00" : row["WTI Fcast "].ToString());
            forecastdata.Brent_Fcast = decimal.Parse(row["Brent Fcast"].ToString() == string.Empty ? "0.00" : row["Brent Fcast"].ToString());
            forecastdata.Steel_HR_MT_Fcast_US = decimal.Parse(row["Steel HR MT Fcast-US"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-US"].ToString());
            forecastdata.Steel_HR_MT_Fcast_EU = decimal.Parse(row["Steel HR MT Fcast-EU"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-EU"].ToString());
            forecastdata.Steel_HR_MT_Fcast_CN = decimal.Parse(row["Steel HR MT Fcast-CN"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-CN"].ToString());
            forecastdata.Steel_HR_MT_Fcast_IN = decimal.Parse(row["Steel HR MT Fcast-IN"].ToString() == string.Empty ? "0.00" : row["Steel HR MT Fcast-IN"].ToString());

            commodityIntelligenceDevEntities.ForecastDatas.Add(forecastdata);
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void UpdateRowinRolling12MonthsPriceStockTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, Rolling12MonthsPrice_Stock rolling12monthspricestockToUpdate, DataRow row, int year, int monthIndex)
        {
            rolling12monthspricestockToUpdate.PricePeriodYear = year;
            rolling12monthspricestockToUpdate.PricePeriodMonth = monthIndex;
            rolling12monthspricestockToUpdate.AluminiumPrice = Convert.ToDecimal(row["Al LME+US MW Premium"]);
            rolling12monthspricestockToUpdate.AluminiumStock = (long)Convert.ToDouble(row["Average of Al LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.CopperPrice = Convert.ToDecimal(row["Average of LME Cu MT"]);
            rolling12monthspricestockToUpdate.CopperStock = (long)Convert.ToDouble(row["Average of Cu LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.SilverPrice = Convert.ToDecimal(row["Average of Silver Comex Price"]);
            rolling12monthspricestockToUpdate.SilverStock = (long)Convert.ToDouble(row["Average of Silver Comex Stocks"].ToString());
            rolling12monthspricestockToUpdate.LeadPrice = Convert.ToDecimal(row["Average of LME Lead MT"]);
            rolling12monthspricestockToUpdate.LeadStock = (long)Convert.ToDouble(row["Average of Lead LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.NickelPrice = Convert.ToDecimal(row["Average of LME Ni MT"]);
            rolling12monthspricestockToUpdate.NickelStock = (long)Convert.ToDouble(row["Average of Ni LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.ZincPrice = Convert.ToDecimal(row["Average of LME Zn MT"]);
            rolling12monthspricestockToUpdate.ZincStock = (long)Convert.ToDouble(row["Average of Zn LME Stocks"].ToString());

            commodityIntelligenceDevEntities.Rolling12MonthsPrice_Stock.Add(rolling12monthspricestockToUpdate);
            commodityIntelligenceDevEntities.Entry(rolling12monthspricestockToUpdate).State = System.Data.Entity.EntityState.Modified;
            commodityIntelligenceDevEntities.SaveChanges();
        }

        public static void AddRowinRolling12MonthsPriceStockTable(CommodityIntelligenceDevEntities commodityIntelligenceDevEntities, DataRow row, int year, int monthIndex)
        {
            Rolling12MonthsPrice_Stock rolling12monthspricestockToUpdate = new Rolling12MonthsPrice_Stock { };
            rolling12monthspricestockToUpdate.PricePeriodYear = year;
            rolling12monthspricestockToUpdate.PricePeriodMonth = monthIndex;
            rolling12monthspricestockToUpdate.AluminiumPrice = Convert.ToDecimal(row["Al LME+US MW Premium"]);
            rolling12monthspricestockToUpdate.AluminiumStock = (long)Convert.ToDouble(row["Average of Al LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.CopperPrice = Convert.ToDecimal(row["Average of LME Cu MT"]);
            rolling12monthspricestockToUpdate.CopperStock = (long)Convert.ToDouble(row["Average of Cu LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.SilverPrice = Convert.ToDecimal(row["Average of Silver Comex Price"]);
            rolling12monthspricestockToUpdate.SilverStock = (long)Convert.ToDouble(row["Average of Silver Comex Stocks"].ToString());
            rolling12monthspricestockToUpdate.LeadPrice = Convert.ToDecimal(row["Average of LME Lead MT"]);
            rolling12monthspricestockToUpdate.LeadStock = (long)Convert.ToDouble(row["Average of Lead LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.NickelPrice = Convert.ToDecimal(row["Average of LME Ni MT"]);
            rolling12monthspricestockToUpdate.NickelStock = (long)Convert.ToDouble(row["Average of Ni LME Stocks"].ToString());
            rolling12monthspricestockToUpdate.ZincPrice = Convert.ToDecimal(row["Average of LME Zn MT"]);
            rolling12monthspricestockToUpdate.ZincStock = (long)Convert.ToDouble(row["Average of Zn LME Stocks"].ToString());

            commodityIntelligenceDevEntities.Rolling12MonthsPrice_Stock.Add(rolling12monthspricestockToUpdate);
            commodityIntelligenceDevEntities.SaveChanges();
        }
    }

    static class Extensions
    {
        public static DataTable GetDataTableFromSpreadSheet(this SpreadsheetDocument document, string sheetname, string tablename)
        {
            var results = new DataTable();
            try
            {
                Sheet sheet = document.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetname).FirstOrDefault();
                WorksheetPart sheetPart = (WorksheetPart)(document.WorkbookPart.GetPartById(sheet.Id));
                foreach (TableDefinitionPart tdp in sheetPart.TableDefinitionParts)
                {
                    if (tdp.Table.Name == tablename)
                    {
                        tdp.Table.AutoFilter = new AutoFilter() { Reference = tdp.Table.Reference };
                        string rangeSet = tdp.Table.Reference;
                        string[] newrangeSet = rangeSet.Split(':');
                        string startcount = "";
                        string endcount = "";
                        string startaddress = "";
                        string endaddress = "";
                        int srt = 0;
                        int ed = 0;

                        char[] start = newrangeSet[0].Where(char.IsLetter).ToArray();
                        foreach (var s in start)
                        {
                            startaddress += s.ToString();
                        }

                        char[] end = newrangeSet[1].Where(char.IsLetter).ToArray();
                        foreach (var e in end)
                        {
                            endaddress += e.ToString();
                        }

                        char[] scount = newrangeSet[0].Where(char.IsNumber).ToArray();
                        foreach (var st in scount)
                        {
                            startcount += st.ToString();
                        }

                        char[] ecount = newrangeSet[1].Where(char.IsNumber).ToArray();
                        foreach (var en in ecount)
                        {
                            endcount += en.ToString();
                        }

                        srt = Convert.ToInt32(startcount);
                        ed = Convert.ToInt32(endcount);

                        List<string> alphaarray = new List<string> { };
                        foreach (var letters in EnumerateLetters(end.Count()))
                        {
                            alphaarray.Add(letters);
                        }

                        int indexOfstart = alphaarray.IndexOf(startaddress);
                        int indexOfend = alphaarray.IndexOf(endaddress);
                        int diffrange = indexOfend - indexOfstart + 1;

                        List<string> reference = alphaarray.GetRange(indexOfstart, diffrange);
                        TableColumns columns = tdp.Table.TableColumns;
                        foreach (var item in columns)
                        {
                            results.Columns.Add(((TableColumn)item).Name.Value);
                        }

                        for (int i = srt; i <= ed; i++)
                        {
                            if (i != srt)
                            {
                                List<string> datarow = new List<string>();
                                foreach (var r in reference)
                                {
                                    string addressname = r + i.ToString();
                                    Cell theCell = sheetPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == addressname).FirstOrDefault();
                                    string cellvalue = GetValue(theCell, document);
                                    datarow.Add(cellvalue);
                                }
                                results.Rows.Add(datarow.ToArray());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //results = new DataTable();
            }
            return results;
        }

        public static string GetValue(this Cell cell, SpreadsheetDocument document)
        {
            string result = string.Empty;
            try
            {
                if (cell != null && cell.ChildElements.Count != 0)
                {
                    var part = document.WorkbookPart.SharedStringTablePart;
                    if (cell.DataType != null && cell.DataType == CellValues.SharedString)
                        result = part.SharedStringTable.ChildElements[int.Parse(cell.CellValue.InnerText)].InnerText;
                    else
                        result = cell.CellValue.InnerText;
                }
            }
            catch (Exception)
            {
                result = string.Empty;
            }
            return result;
        }

        public static DateTime ConvertToDateTime(string strExcelDate)
        {
            double excelDate;
            try
            {
                excelDate = Convert.ToDouble(strExcelDate);
            }
            catch
            {
                return DateTime.Now;
            }
            if (excelDate < 1)
            {
                throw new ArgumentException("Excel dates cannot be smaller than 0.");
            }
            DateTime dateOfReference = new DateTime(1900, 1, 1);
            if (excelDate > 60d)
            {
                excelDate = excelDate - 2;
            }
            else
            {
                excelDate = excelDate - 1;
            }
            return dateOfReference.AddDays(excelDate);
        }

        public static IEnumerable<string> EnumerateLetters(int length)
        {
            for (int i = 1; i <= length; i++)
            {
                foreach (var letters in EnumerateLettersExact(i))
                {
                    yield return letters;
                }
            }
        }

        public static IEnumerable<string> EnumerateLettersExact(int length)
        {
            if (length == 0)
            {
                yield return "";
            }
            else
            {
                for (char c = 'A'; c <= 'Z'; ++c)
                {
                    foreach (var letters in EnumerateLettersExact(length - 1))
                    {
                        yield return c + letters;
                    }
                }
            }
        }
    }
}