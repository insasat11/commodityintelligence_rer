﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.EventReceivers;
using static CommodityIntelligence_ExceltoDBUploadWeb.DBMethods;

namespace CommodityIntelligence_ExceltoDBUploadWeb.Services
{
    public class AppEventReceiver : IRemoteEventService
    {
        /// <summary>
        /// Handles app events that occur after the app is installed or upgraded, or when app is being uninstalled.
        /// </summary>
        /// <param name="properties">Holds information about the app event.</param>
        /// <returns>Holds information returned from the app event.</returns>
        public SPRemoteEventResult ProcessEvent(SPRemoteEventProperties properties)
        {
            SPRemoteEventResult result = new SPRemoteEventResult();

            using (ClientContext clientContext = TokenHelper.CreateAppEventClientContext(properties, useAppWeb: false))
            {
                if (clientContext != null)
                {
                    clientContext.Load(clientContext.Web);
                    clientContext.ExecuteQuery();
                }
            }

            return result;
        }

        /// <summary>
        /// This method is a required placeholder, but is not used by app events.
        /// </summary>
        /// <param name="properties">Unused.</param>
        public void ProcessOneWayEvent(SPRemoteEventProperties properties)
        {
            //throw new NotImplementedException();
            if (properties.EventType == SPRemoteEventType.ItemAdded)
            {
                Trace.TraceError("RER hits for added");
                SPRemoteEventResult result = new SPRemoteEventResult();
                CommodityIntelligenceDevEntities commodityIntelligenceDevEntities = new CommodityIntelligenceDevEntities();
                var a = commodityIntelligenceDevEntities.CommoditySummaries.ToList();
                Trace.TraceError("RER hits for commodityIntelligenceDevEntities");

                Uri siteUri = new Uri(ConfigurationManager.AppSettings["SiteUrl"]);
                string realm = TokenHelper.GetRealmFromTargetUrl(siteUri);
                var token = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, siteUri.Authority, realm).AccessToken;

                using (var clientContext = TokenHelper.GetClientContextWithAccessToken(siteUri.ToString(), token))
                {
                    Trace.TraceError("RER hits for ctx");
                    if (clientContext != null)
                    {
                        Trace.TraceError("RER hits for ctx not null");
                        clientContext.Load(clientContext.Web);
                        clientContext.ExecuteQuery();
                        string itemid = Convert.ToString(properties.ItemEventProperties.ListItemId);
                        Trace.TraceError(itemid);
                        Trace.TraceError("RER hits for added");

                        List CommodityIntelligenceList = clientContext.Web.Lists.GetByTitle(ConfigurationManager.AppSettings["ListName"]);
                        clientContext.Load(CommodityIntelligenceList);
                        clientContext.ExecuteQuery();
                        Trace.TraceError("list is loaded");

                        ListItem listItem = CommodityIntelligenceList.GetItemById(itemid);
                        clientContext.Load(listItem);
                        clientContext.ExecuteQuery();
                        Trace.TraceError("listitem is loaded");

                        File file = listItem.File;
                        clientContext.Load(file);
                        clientContext.ExecuteQuery();
                        Trace.TraceError("file is loaded");

                        try
                        {
                            DataTable dataTable = new DataTable();

                            ClientResult<System.IO.Stream> data = file.OpenBinaryStream();
                            clientContext.Load(file);
                            clientContext.ExecuteQuery();
                            Trace.TraceError("openbinary is loaded");

                            System.IO.MemoryStream mStream = new System.IO.MemoryStream();
                            data.Value.CopyTo(mStream);

                            SpreadsheetDocument document = SpreadsheetDocument.Open(mStream, false);
                            string[] monthnames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

                            var commoditysummarydt = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("Commodity Summary for App", "AppSum_Table");

                            if (commoditysummarydt.Rows.Count > 0)
                            {
                                foreach (DataRow row in commoditysummarydt.Rows)
                                {
                                    string commodity = Convert.ToString(row["Commodity"]);
                                    var commodityToUpdate = commodityIntelligenceDevEntities.CommoditySummaries.Where(cm => cm.Commodity == commodity).FirstOrDefault();
                                    if (commodityToUpdate != null)
                                    {
                                        UpdateRowinCommoditySummaryTable(commodityIntelligenceDevEntities, commodityToUpdate, row);
                                    }

                                    else
                                    {
                                        AddRowinCommoditySummaryTable(commodityIntelligenceDevEntities, row);
                                    }
                                }
                                Trace.TraceError("db row is added for commodity summary for app");
                            }
                            else
                            {
                                Console.WriteLine("Commodity Summary for App table has some issue");
                            }

                            var polymerdt = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("MT CHARTS", "Thermoplastics_Table");

                            if (polymerdt.Rows.Count > 0)
                            {
                                foreach (DataRow row in polymerdt.Rows)
                                {
                                    int year = Convert.ToInt32(row["YEAR"]);
                                    string month = Convert.ToString(row["MONTH"]);
                                    int monthIndex = Array.IndexOf(monthnames, month) + 1;
                                    var polymerToUpdate = commodityIntelligenceDevEntities.Polymers.Where(cm => cm.PricePeriodYear == year && cm.PricePeriodMonth == monthIndex).FirstOrDefault();
                                    if (polymerToUpdate != null)
                                    {
                                        UpdateRowinPolymerTable(commodityIntelligenceDevEntities, polymerToUpdate, row, year, monthIndex);
                                    }

                                    else
                                    {
                                        AddRowinPolymerTable(commodityIntelligenceDevEntities, row, year, monthIndex);
                                    }
                                }
                                Trace.TraceError("db row is added for polymer");
                            }
                            else
                            {
                                Console.WriteLine("Polymer table has some issue");
                            }


                            var polyamidesdt = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("MT CHARTS", "Polyamides_Table");

                            if (polyamidesdt.Rows.Count > 0)
                            {
                                foreach (DataRow row in polyamidesdt.Rows)
                                {
                                    int year = Convert.ToInt32(row["YEAR"]);
                                    string month = Convert.ToString(row["MONTH"]);
                                    int monthIndex = Array.IndexOf(monthnames, month) + 1;
                                    var polyamidesToUpdate = commodityIntelligenceDevEntities.Polyamides.Where(cm => cm.PricePeriodYear == year && cm.PricePeriodMonth == monthIndex).FirstOrDefault();
                                    if (polyamidesToUpdate != null)
                                    {
                                        UpdateRowinPolyamidesTable(commodityIntelligenceDevEntities, polyamidesToUpdate, row, year, monthIndex);
                                    }

                                    else
                                    {
                                        AddRowinPolyamidesTable(commodityIntelligenceDevEntities, row, year, monthIndex);
                                    }

                                    var polyamidesforecastToUpdate = commodityIntelligenceDevEntities.Polyamides_Forecast.Where(cm => cm.PricePeriodYear == year && cm.PricePeriodMonth == monthIndex).FirstOrDefault();
                                    if (polyamidesforecastToUpdate != null)
                                    {
                                        UpdateRowinPolyamidesForeCastTable(commodityIntelligenceDevEntities, polyamidesforecastToUpdate, row, year, monthIndex);
                                    }

                                    else
                                    {
                                        AddRowinPolyamidesForeCastTable(commodityIntelligenceDevEntities, row, year, monthIndex);
                                    }
                                }
                                Trace.TraceError("db row is added for ployamides table");
                            }
                            else
                            {
                                Console.WriteLine("Polyamides table has some issue");
                            }

                            var epoxy = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("MT CHARTS", "Epoxy_Table");
                            var resin = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("MT CHARTS", "Resin_Table");
                            epoxy.Columns.Add(resin.Columns["UP resin"].ColumnName, resin.Columns["UP resin"].DataType);
                            epoxy.Columns.Add(resin.Columns["Glass Fibers"].ColumnName, resin.Columns["Glass Fibers"].DataType);

                            foreach (DataRow row in epoxy.Rows)
                            {
                                foreach (DataRow r in resin.Rows)
                                {
                                    if (row["YEAR"].ToString() == r["YEAR"].ToString() && row["MONTH"].ToString() == r["MONTH"].ToString())
                                    {
                                        row["UP resin"] = r["UP resin"].ToString();
                                        row["Glass Fibers"] = r["Glass Fibers"].ToString();
                                    }
                                }
                            }

                            if (epoxy.Rows.Count > 0)
                            {
                                foreach (DataRow row in epoxy.Rows)
                                {
                                    int year = Convert.ToInt32(row["YEAR"]);
                                    string month = Convert.ToString(row["MONTH"]);
                                    int monthIndex = Array.IndexOf(monthnames, month) + 1;
                                    var epoxyresinToUpdate = commodityIntelligenceDevEntities.Epoxy_Composites.Where(cm => cm.PricePeriodYear == year && cm.PricePeriodMonth == monthIndex).FirstOrDefault();
                                    if (epoxyresinToUpdate != null)
                                    {
                                        UpdateRowinEpoxyResinTable(commodityIntelligenceDevEntities, epoxyresinToUpdate, row, year, monthIndex);
                                    }

                                    else
                                    {
                                        AddRowinEpoxyResinTable(commodityIntelligenceDevEntities, row, year, monthIndex);
                                    }
                                }
                                Trace.TraceError("db row is added for epoxy table");
                            }
                            else
                            {
                                Console.WriteLine("Epoxy_Table table has some issue");
                            }

                            var budgetforecast = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("BUDGETS-FCASTS", "LKUP_FCASTS_ABB");

                            if (budgetforecast.Rows.Count > 0)
                            {
                                foreach (DataRow row in budgetforecast.Rows)
                                {
                                    DateTime month = Extensions.ConvertToDateTime(row["Month"].ToString());
                                    var budgetforecastToUpdate = commodityIntelligenceDevEntities.ForecastDatas.Where(cm => cm.MonthYear == month).FirstOrDefault();
                                    if (budgetforecastToUpdate != null)
                                    {
                                        UpdateRowinBudgetForcastTable(commodityIntelligenceDevEntities, budgetforecastToUpdate, row);
                                    }

                                    else
                                    {
                                        AddRowinBudgetForcastTable(commodityIntelligenceDevEntities, row);
                                    }
                                }
                                Trace.TraceError("db row is added for BUDGETS-FCASTS table");
                            }
                            else
                            {
                                Console.WriteLine("LKUP_FCASTS_ABB table has some issue");
                            }

                            var rolling12monthspricestock = SpreadsheetDocument.Open(mStream, false).GetDataTableFromSpreadSheet("MT CHARTS", "Metals_Table");

                            if (rolling12monthspricestock.Rows.Count > 0)
                            {
                                foreach (DataRow row in rolling12monthspricestock.Rows)
                                {
                                    int year = Convert.ToInt32(row["YEAR"]);
                                    string month = Convert.ToString(row["MONTH"]);
                                    int monthIndex = Array.IndexOf(monthnames, month) + 1;
                                    var rolling12monthspricestockToUpdate = commodityIntelligenceDevEntities.Rolling12MonthsPrice_Stock.Where(cm => cm.PricePeriodYear == year && cm.PricePeriodMonth == monthIndex).FirstOrDefault();
                                    if (rolling12monthspricestockToUpdate != null)
                                    {
                                        UpdateRowinRolling12MonthsPriceStockTable(commodityIntelligenceDevEntities, rolling12monthspricestockToUpdate, row, year, monthIndex);
                                    }

                                    else
                                    {
                                        AddRowinRolling12MonthsPriceStockTable(commodityIntelligenceDevEntities, row, year, monthIndex);
                                    }
                                }
                                Trace.TraceError("db row is added for Rolling12MonthsPriceStock table");
                            }
                            else
                            {
                                Console.WriteLine("Metals_Table table has some issue");
                            }
                        }
                        catch (DbEntityValidationException e)
                        {
                            var newException = new FormattedDbEntityValidationException(e);
                        }
                    }
                }
            }
        }
    }
}